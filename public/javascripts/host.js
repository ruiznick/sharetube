var player;

var searchBar
var searchResults

var appData = {
    playerState: 1
}


function createResultEntry(item) {
    var tItem = "<a href='/manage/watch/" + item.id.videoId + "'><div class='card'>"

    tItem += "<img class='card-img-top' src='" + item.snippet.thumbnails.medium.url + "'/>"
    tItem += "<div class='card-body'>"
    tItem += "<h5 class='card-title'>" + item.snippet.title + "</h5>"
    tItem += "<h6 class='card-subtitle mb-2 text-muted'>" + item.snippet.channelTitle + "</h6>"
    tItem += "</div>"

    tItem += "</div></a>"
    return tItem
}

function handleSearch() {
    // Show that the results are loading
    searchResults.innerHTML = "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>"


    var query = encodeURIComponent(searchBar.value).replace(/%20/, "+")
    var request = gapi.client.youtube.search.list({
        part: "snippet",
        type: "video",
        q: query,
        maxResults: 25,
        order: "viewCount"
    })

    request.execute(function(res) {
        var items = res.result.items
        var tResults = "<div class='card-columns'>"

        for( var i=0; i < items.length; i++) {
            tResults += createResultEntry(items[i])
        }

        tResults += "</div>"
        searchResults.innerHTML = tResults
    })
}

function ready() {
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    searchResults = document.getElementById("searchResults")

    searchBar = document.getElementById("searchBar")
    searchBar.addEventListener("keyup", function (e) {
        if (e.keyCode === 13) {
            handleSearch()
        }
    })

    var searchButton = document.getElementById("searchButton")
    searchButton.addEventListener("click", handleSearch)
}

function onYouTubeIframeAPIReady() {
    var request = new XMLHttpRequest()
    request.open("GET", "/api/session")

    request.onload = function() {

        if (this.status >= 200 && this.status < 400) {
            appData.session = JSON.parse(this.response)

            if (appData.session.host.videoId.length > 0) {

                var copyInfo = "<a tabindex='0' class='btn btn-outline-success' role='button' data-toggle='popover' data-trigger='focus' data-content='Share this link to let others watch the same YouTube video. Changing videos, pausing, and skipping ahead will instantly be mirrored to those with this link.'><i class='fa fa-info-circle'></i></a>"
                var copyButton = "<div class='input-group input-group-sm'><span class='input-group-btn'><button class='btn btn-outline-success' type='button' id='copyButton'>Copy Link</button></span><input id='linkInput' type='text' class='form-control' value='http://sharetube.ruiznick.com/watch/" + appData.session.host.userId + "' readonly>" + copyInfo + "</div>"

                document.getElementById("copyDiv").innerHTML = copyButton
                document.getElementById("copyButton").addEventListener("click", copyLink)

                $('[data-toggle="popover"]').popover();

                player = new YT.Player('player', {
                    height: '390',
                    width: '640',
                    videoId: appData.session.host.videoId,
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    },
                    playerVars: {
                        iv_load_policy: 3,
                        rel: 0
                    }
                });
            }
        }
    }

    request.send()
}

function copyLink() {
    var copyText = document.getElementById("linkInput")
    copyText.select()
    document.execCommand("Copy")
}

function onPlayerReady(event) {
        event.target.playVideo();
}

function setServerTimestamp() {
    var timestampData = JSON.stringify({
        timestamp: new Date(),
        startSeconds: player.getCurrentTime(),
        playerState: appData.playerState
    })
    var request = new XMLHttpRequest()
    request.open("POST", "/manage/timestamp")
    request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    request.send(timestampData)
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING) {
        appData.playerState = 1
        setServerTimestamp()
    }

    if (event.data == YT.PlayerState.PAUSED) {
        appData.playerState = 2
        setServerTimestamp()
    }
}

function stopVideo() {
    player.stopVideo();
}

function init() {
    gapi.client.setApiKey("AIzaSyAa0QAHDm8y589N3r89zLT2immPN2BjR5k")
    gapi.client.load("youtube", "v3", ready)
}

