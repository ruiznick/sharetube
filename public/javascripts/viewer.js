var player;

var appData = {
    isPaused: false
}

function updateAppData(e) {
    var newData = JSON.parse(e.data)

    if (!appData.session) {
        appData.session = {
            viewer: {
                timestamp: new Date(),
                startSeconds: 0,
                videoId: "",
                playerState: 1
            }
        }
    }
    appData.session.viewer.previousVideoId = appData.session.viewer.videoId

    appData.session.viewer.timestamp = new Date(newData.timestamp)
    appData.session.viewer.startSeconds = newData.startSeconds
    appData.session.viewer.videoId = newData.videoId
    appData.session.viewer.playerState = newData.playerState

    seekToHost()
}

function ready() {
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    appData.sourceEvent = new EventSource("/stream")
    appData.sourceEvent.addEventListener("message", updateAppData, false)
}

function onYouTubeIframeAPIReady() {
    var request = new XMLHttpRequest()
    request.open("GET", "/api/session")

    request.onload = function() {

        if (this.status >= 200 && this.status < 400) {
            appData.session = JSON.parse(this.response)
            appData.session.viewer.previousVideoId = ""

            if (appData.session.viewer.videoId.length > 0) {

                player = new YT.Player('player', {
                    height: '390',
                    width: '640',
                    videoId: appData.session.viewer.videoId,
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    },
                    playerVars: {
                        iv_load_policy: 3,
                        rel: 0
                    }
                });
            }
        }
    }

    request.send()
}

function seekToHost() {

    if (!player) {
        return
    }

    var currentTime = new Date()
    var hostTime = new Date(appData.session.viewer.timestamp)
    var startSeconds =  appData.session.viewer.startSeconds
    var targetSeconds = Math.floor((currentTime - hostTime) / 1000) + startSeconds

    if (appData.session.viewer.videoId != appData.session.viewer.previousVideoId &&
        appData.session.viewer.previousVideoId.length > 0) {
        player.loadVideoById(appData.session.viewer.videoId, targetSeconds)
    } else {
        if (appData.session.viewer.playerState === 1) {
            player.seekTo(targetSeconds, true)
        } else if (appData.session.viewer.playerState === 2) {
            player.seekTo(startSeconds, true)
        }
    }

    if (appData.session.viewer.playerState === 1 && player.getPlayerState() !== 1) {
        player.playVideo()
    } else if (appData.session.viewer.playerState === 2 && player.getPlayerState() !== 2) {
        player.pauseVideo()
    }
}

function onPlayerReady(event) {
    if (appData.session.viewer.playerState == 1) {
        event.target.playVideo();
    }
    seekToHost()
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && appData.isPaused) {
        appData.isPaused = false
        seekToHost()
    }

    if (event.data == YT.PlayerState.PAUSED) {
        appData.isPaused = true
    }
}

function stopVideo() {
    player.stopVideo();
}

