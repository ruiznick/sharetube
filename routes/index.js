var express = require('express');
var router = express.Router();
var path = require("path")

var UserDB = {}

function getVideoId(userId) {
    if (UserDB[userId]) {
        return UserDB[userId].videoId
    }
    return ""
}

function getTimestamp(userId) {
    if (UserDB[userId]) {
        return UserDB[userId].timestamp
    }
    return new Date()
}

function getStartSeconds(userId) {
    if (UserDB[userId]) {
        return UserDB[userId].startSeconds
    }
    return 0
}

// 1 = playing, 2 = paused
function getPlayerState(userId) {
    if (UserDB[userId]) {
        return UserDB[userId].playerState
    }
    return 1
}

function getRandomNumber() {
    return Math.floor(Math.random() * (899999)) + 100000
}

function callConnections(userId) {
    if (UserDB[userId]) {
        var tData = {
            videoId: UserDB[userId].videoId,
            timestamp: UserDB[userId].timestamp,
            startSeconds: UserDB[userId].startSeconds,
            playerState: UserDB[userId].playerState
        }
       for (var i=0; i < UserDB[userId].connections.length; i++) {
            UserDB[userId].connections[i].sseSend(tData)
       }
    }

}

function pushSSEConnection(userId, res) {
    if (!UserDB[userId]) {
        return
    }

    UserDB[userId].connections.push(res)
}

function getUniqueUserId() {
    var nextId = getRandomNumber()

    while(nextId.toString() in UserDB) {
        nextId = getRandomNumber()
    }
    return nextId
}

function sessionInit(req, res, next) {
    if (!req.session.user) {
        req.session.user = {
            host: {
                userId: "",
                videoId: "",
                timestamp: new Date(),
                startSeconds: 0,
                playerState: 1
            },
            viewer: {
                userId: "",
                videoId: "",
                timestamp: new Date(),
                startSeconds: 0,
                playerState: 1
            }
        }
    }
   next()
}

/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname, "../public", "main.html"))
});

router.get('/watch/:userId', sessionInit, function(req, res, next) {
    req.session.user.viewer.userId = req.params.userId
    req.session.user.viewer.videoId = getVideoId(req.params.userId)
    req.session.user.viewer.timestamp = getTimestamp(req.params.userId)
    req.session.user.viewer.startSeconds = getStartSeconds(req.params.userId)
    req.session.user.viewer.playerState = getPlayerState(req.params.userId)
    res.sendFile(path.join(__dirname, "../public", "viewer.html"))
});

// SSE open connection
router.get("/stream", sessionInit, function (req, res, next) {
    res.sseSetup()
    res.sseSend({test: "hello"})
    pushSSEConnection(req.session.user.viewer.userId, res)
})

router.get("/manage", sessionInit, function (req, res, next) {

    if (req.session.user.host.userId.length <= 0) {
        var tNewUserId = getUniqueUserId()
        req.session.user.host.userId = tNewUserId
        req.session.user.host.videoId = ""

        UserDB[tNewUserId] = {
            videoId: "",
            timestamp: new Date(),
            connections: [],
            startSeconds: 0,
            playerState: 1
        }
    }

    res.sendFile(path.join(__dirname, "../public", "host.html"))
})

router.post("/manage/timestamp", sessionInit, function (req, res, next) {

    var newDate = new Date(req.body.timestamp)
    var newStart = Math.floor(req.body.startSeconds)
    var newPlayerState = Math.floor(req.body.playerState)

    if (newDate && Number.isInteger(newStart) && Number.isInteger(newPlayerState)) {
        if (UserDB[req.session.user.host.userId]) {
            UserDB[req.session.user.host.userId].timestamp = newDate
            UserDB[req.session.user.host.userId].startSeconds = newStart
            UserDB[req.session.user.host.userId].playerState = newPlayerState
            req.session.user.host.timestamp = newDate
            req.session.user.host.startSeconds = newStart
            req.session.user.host.playerState = newPlayerState

            callConnections(req.session.user.host.userId)
        }
    }

    res.send(" ")
})

router.get("/manage/watch/:videoId", sessionInit, function (req, res, next) {
    if (UserDB[req.session.user.host.userId]) {
        UserDB[req.session.user.host.userId].videoId = req.params.videoId
        UserDB[req.session.user.host.userId].timestamp = new Date()
        UserDB[req.session.user.host.userId].startSeconds = 0
        UserDB[req.session.user.host.userId].playerState = 1

        req.session.user.host.videoId = req.params.videoId
        req.session.user.host.timestamp = UserDB[req.session.user.host.userId].timestamp
        req.session.user.host.startSeconds = 0
        req.session.user.host.playerState = 1

        callConnections(req.session.user.host.userId)

        res.sendFile(path.join(__dirname, "../public", "host.html"))
    } else {
        res.redirect("/manage")
    }
})

router.get("/api/session", sessionInit, function (req, res, next) {
    res.jsonp(req.session.user)
})


module.exports = router;
